#include "PowerUp.h"

PowerUp::PowerUp()
{
	
	SetPosition(0, 0); //Default position is set to x = 0 and y = 0, top left of the screen.
	SetCollisionRadius(30); //May need to adjust so items are easier to pick up. Should be large so it's easy to collect.

	//these attributes are a work in progress, should be updated to be easy to collect.
	m_speed = 300; 
	m_isInvulnurable = true; 
}

void PowerUp::Update(const GameTime* pGameTime) 
{

}

void PowerUp::Hit(const float damage)
{

	if (m_isInvulnurable)
	{
		//Sets the value back to itself so that the object cannot be deleted by shot.
		m_hitPoints = m_hitPoints;

		
	}
}

//Sets the hitpoints of the object.
void PowerUp::Initialize(const Vector2 position, const double delaySeconds)
{
	m_hitPoints = m_maxHitPoints;
}