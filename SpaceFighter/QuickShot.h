#pragma once

#include "Weapon.h"
#include "Blaster.h"



//Sets attributes of the Weapon when set in QuickShot Mode.
class QuickShot : public Weapon
{
public:

	QuickShot(const bool isActive) : Weapon(isActive)
	{
		//Changed the player's attack speed!! from m_cooldownSeconds = 0.35 to 0.1 
		m_cooldown = 0;
		m_cooldownSeconds = 0.15;

		Deactivate();

	}

	virtual ~QuickShot() { }


	virtual void Update(const GameTime* pGameTime)
	{
		if (m_cooldown > 0) m_cooldown -= pGameTime->GetTimeElapsed();
	}

	virtual bool CanFire() const { return m_cooldown <= 0; }

	virtual void ResetCooldown() { m_cooldown = 0; }

	virtual float GetCooldownSeconds() { return m_cooldownSeconds; }

	virtual void SetCooldownSeconds(const float seconds) { m_cooldownSeconds = seconds; }


	virtual void Fire(TriggerType triggerType)
	{
		if (IsActive() && CanFire())
		{
			if (triggerType.Contains(GetTriggerType()))
			{
				Projectile* pProjectile = GetProjectile();
				if (pProjectile)
				{
					pProjectile->Activate(GetPosition(), true);
					m_cooldown = m_cooldownSeconds;
				}
			}
		}
	}


	//Used to limit how long the power-up lasts for when power-up is collected.
	virtual float Timer() const { return m_timer; }


private:

	float m_cooldown;
	float m_cooldownSeconds;

	//Used to limit how long the power-up lasts for.
	float m_timer;

};

