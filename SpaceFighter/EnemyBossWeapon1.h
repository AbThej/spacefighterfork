#pragma once

#include "EnemyShip.h"

class EnemyBossWeapon1 : public EnemyShip
{

public:
	EnemyBossWeapon1();
	virtual ~EnemyBossWeapon1() { }

	void SetTexture(Texture* pTexture) { m_pTexture = pTexture; }

	virtual void Update(const GameTime* pGameTime);

	virtual void Draw(SpriteBatch* pSpriteBatch);

	virtual void Hit(const float damage);

private:

	Texture* m_pTexture;

	int m_hitFrames;

};

