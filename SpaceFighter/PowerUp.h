#pragma once

#include "GameObject.h"
#include "Weapon.h"


/*Properties the powerup's contain.
* - Update
* - Draw
* - Invulnurability
* - Speed
* - Delay Timer
* - Hitpoints (This should be used to despawn the object upon coliding with the player.
*/ 

class PowerUp : public GameObject
{

public:
	
	//Calls an instance of the PowerUp Method and a despawn virtual method.
	PowerUp();
	virtual ~PowerUp() {}

	//Update function that uses constant of gametime to determine it's length.
	virtual void Update(const GameTime* pGameTime);

	//Pure Virtual Function, used for drawing the sprite.
	virtual void Draw(SpriteBatch* pSpriteBatch) = 0; //Used to determine to draw a sprite batch.

	//Used for detecting hits with a damage float variable.
	virtual void Hit(const float damage);

	//Used to initialize the object.
	virtual void Initialize(const Vector2 position, const double delaySeconds);

	//Used to run the IsInvulerable command.
	virtual bool IsInvulnerable() const { return m_isInvulnurable; } //Used to set properties so that the powerup can not be shot. 

	//Sets the invulnurability of the powerup object to true.
	virtual void SetInvulnurable(bool isInvulnurable = true) { m_isInvulnurable = isInvulnurable; }

	//Used to return "Power Up" as a string to the std::string.
	virtual std::string ToString() const { return "PowerUp"; }



	//Used to determine what kind of collision type to use for the powerups. NEEDS TO BE FIXED UP.

	virtual CollisionType GetCollisionType() const = 0;


	virtual float GetSpeed() const { return m_speed; }

	virtual void SetSpeed(const float speed) { m_speed = speed; }

	//Uncomment if needed again
	//virtual void SetMaxHitPoints(const float hitPoints) { m_maxHitPoints = hitPoints; }


protected:

	virtual float GetHitPoints() const { return m_hitPoints; }

	//Uncomment if needed again.
	//virtual float GetMaxHitPoints() const { return m_maxHitPoints; }

private:
	
	//Float used for setting speed
	float m_speed;

	//Hit points, may go unused, but might be used for detecting collision with player.
	float m_hitPoints;
	float m_maxHitPoints;

	//Boolean used for determining invulnurability.
	bool m_isInvulnurable;

	//Used to define the delaySeconds and activationSeconds doubles for the object.
	double m_delaySeconds;
	double m_activationSeconds;

	std::vector<Weapon*> m_weapons;
	std::vector<Weapon*>::iterator m_weaponIt;
};



//Use this in the specific powerups.
//Used to get the value relating to the delayseconds.
//virtual double GetDelaySeconds() const { return m_delaySeconds; }

//Use in the specific collisions.
//virtual CollisionType GetCollisionType() const { return CollisionType::ENEMY | CollisionType::POWERUP; } //Uses collision type 4 as that is what is set
