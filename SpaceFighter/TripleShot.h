#pragma once

#include "Weapon.h"


//Used to define attributes of the tripleshot mode for the weapon.

class TripleShot : public Weapon
{

public:

	TripleShot(const bool isActive) : Weapon(isActive)
	{
		//Changed the player's attack speed!! from m_cooldownSeconds = 0.35 to 0.1 
		m_cooldown = 0;
		m_cooldownSeconds = 0.35;
	}

	virtual ~TripleShot() { }


	virtual void Update(const GameTime* pGameTime)
	{
		if (m_cooldown > 0) m_cooldown -= pGameTime->GetTimeElapsed();
	}

	virtual bool CanFire() const { return m_cooldown <= 0; }

	virtual void ResetCooldown() { m_cooldown = 0; }

	virtual float GetCooldownSeconds() { return m_cooldownSeconds; }

	virtual void SetCooldownSeconds(const float seconds) { m_cooldownSeconds = seconds; }


	virtual void Fire(TriggerType triggerType)
	{
		if (IsActive() && CanFire())
		{
			if (triggerType.Contains(GetTriggerType()))
			{
				Projectile* pProjectile = GetProjectile();
				if (pProjectile)
				{
					pProjectile->Activate(GetPosition(), true);
					m_cooldown = m_cooldownSeconds;
				}
			}
		}
	}



private:
	float m_cooldown;
	float m_cooldownSeconds;


	float m_timer;
	int m_bulletNumber; //Use in some way to spawn multiple bullets upon a shot.

};

