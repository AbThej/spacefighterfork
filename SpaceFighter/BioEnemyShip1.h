#pragma once

#include "EnemyShip.h"

class BioEnemyShip1 : public EnemyShip
{

public:

	BioEnemyShip1();
	virtual ~BioEnemyShip1() { }

	void SetTexture(Texture* pTexture) { m_pTexture = pTexture; }

	virtual void Update(const GameTime* pGameTime);

	virtual void Draw(SpriteBatch* pSpriteBatch);


private:

	Texture* m_pTexture;

};
