#pragma once

#include "Level.h"



/*THIS IS JUST HEADER FILE FOR THE TEST LEVEL, FEEL FREE TO EDIT IF NEEDING TO TEST A SPECIFIC THING.*/

class LevelTest
{

	LevelTest() { }

	virtual ~LevelTest() { }

	virtual void LoadContent(ResourceManager* pResourceManager);

	virtual void UnloadContent() { }

};

