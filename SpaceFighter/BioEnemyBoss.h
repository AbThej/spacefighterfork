#pragma once

#include "EnemyShip.h"

class BioEnemyBoss : public EnemyShip
{
	
public:
	BioEnemyBoss();
	virtual ~BioEnemyBoss() { }
	
	void SetTexture(Texture * pTexture) { m_pTexture = pTexture; }
	
	virtual void Update(const GameTime * pGameTime);
	
	virtual void Draw(SpriteBatch * pSpriteBatch);

	virtual void Hit(const float damage);
		
private:

	Texture * m_pTexture;
	
	int m_hitFrames;

	
};
