
#include "BioEnemyShip1.h"

BioEnemyShip1::BioEnemyShip1()
{
	SetSpeed(300);
	SetMaxHitPoints(1);
	SetCollisionRadius(20);
}

void BioEnemyShip1::Update(const GameTime* pGameTime)
{
	// If alive
	if (IsActive())
	{
		float x = sin(pGameTime->GetTotalTime() * Math::PI + GetIndex()); //Check time since game starts
		x *= GetSpeed() * pGameTime->GetTimeElapsed() * 1.0f; //Set enemy ship wiggle distance and speed at which they wiggle //Normal is 1.4
		TranslatePosition(x, GetSpeed() * pGameTime->GetTimeElapsed());

		if (!IsOnScreen()) Deactivate(); // if not on screen deactivate
	}

	EnemyShip::Update(pGameTime);
}

void BioEnemyShip1::Draw(SpriteBatch* pSpriteBatch)
{
	if (IsActive())
	{
		pSpriteBatch->Draw(m_pTexture, GetPosition(), Color::White, m_pTexture->GetCenter(), Vector2::ONE, Math::PI, 1);
	}
}