#include "GameEndScreen.h"
#include "GameplayScreen.h"
#include <string>

void OnPlayAgainSelect(MenuScreen* pScreen) 
{
	pScreen->GetScreenManager()->AddScreen(new GameplayScreen());
}

void OnEndGameSelect(MenuScreen* pScreen) 
{
	GameEndScreen* pGameEndScreen = (GameEndScreen*)pScreen;
	pGameEndScreen->SetQuitFlag();
	pGameEndScreen->Exit();
}

void OnRemoveScreen(Screen* pScreen)
{
	GameEndScreen* pGameEndScreen = (GameEndScreen*)pScreen;
	if (pGameEndScreen->IsQuittingGame())
	{
		pScreen->GetGame()->Quit();
	}
}

GameEndScreen::GameEndScreen()
{
	m_pTexture = nullptr;
	SetRemoveCallback(OnRemoveScreen);
	SetTransitionInTime(1.0f);
	SetTransitionOutTime(0.5f);
	Show();
	
};

void GameEndScreen::LoadContent(ResourceManager* pResourceManager)
{

	m_pTexture = pResourceManager->Load<Texture>("Textures\\losingscreen.png");
	m_texturePosition = Game::GetScreenCenter() - Vector2::UNIT_Y * 150;

	const int COUNT = 2;
	MenuItem* pItem;
	Font::SetLoadSize(36, true);
	Font* pFont = pResourceManager->Load<Font>("Fonts\\arialbd.ttf");

	SetDisplayCount(COUNT);
	enum Items { TRY_AGAIN, QUIT };
	std::string text[COUNT] = { "Play Again", "Quit" };

	for (int i = 0; i < COUNT; i++)
	{
		pItem = new MenuItem(text[i]);
		pItem->SetPosition(Vector2(700, 500 + 50 * i));
		pItem->SetFont(pFont);
		pItem->SetColor(Color::Red);
		pItem->SetSelected(i == 0);
		AddMenuItem(pItem);
	}

	GetMenuItem(TRY_AGAIN)->SetSelectCallback(OnPlayAgainSelect);
	GetMenuItem(QUIT)->SetSelectCallback(OnEndGameSelect);
}

void GameEndScreen::Update(const GameTime* pGameTime)
{
	MenuItem* pItem;

	// Set the menu item colors
	for (int i = 0; i < GetDisplayCount(); i++)
	{
		pItem = GetMenuItem(i);
		pItem->SetAlpha(GetAlpha());

		if (pItem->IsSelected()) pItem->SetColor(Color::Green); //Selected Item change from WHITE to GREEN
		else pItem->SetColor(Color::Red); //Not selected item change from BLUE to RED
	}

	MenuScreen::Update(pGameTime);
}

void GameEndScreen::Draw(SpriteBatch* pSpriteBatch)
{
	pSpriteBatch->Begin();
	pSpriteBatch->Draw(m_pTexture, m_texturePosition, Color::White * GetAlpha(), m_pTexture->GetCenter());
	pSpriteBatch->End();

	MenuScreen::Draw(pSpriteBatch);
}
