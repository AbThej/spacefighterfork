
#include "Level01.h"
#include "BioEnemyShip.h"
#include "BioEnemyShip1.h"
#include "BioEnemyBoss.h"
#include "EnemyBossWeapon1.h"
#include "BioEnemyMoon.h"


#include "PowerUp.h" //delete later as specific powerups are needed.
#include "QuickShotPanel.h"
#include "QuickShot.h"

//Removed as the tripleshot was not included.
//#include "TripleShotPanel.h"
//#include "TripleShot.h"



void Level01::LoadContent(ResourceManager* pResourceManager)
{
	// Setup Textures for all enemies
	Texture* pTexture = pResourceManager->Load<Texture>("Textures\\rock1.png");
	Texture* pTexture1 = pResourceManager->Load<Texture>("Textures\\BioEnemyShip.png");
	Texture* pTexture2 = pResourceManager->Load<Texture>("Textures\\BioEnemyBoss.png");
	Texture* pTexture3 = pResourceManager->Load<Texture>("Textures\\EnemyBossWeapon1.png");
	Texture* pTexture4 = pResourceManager->Load<Texture>("Textures\\BioEnemyMoon.png");

	// Setup powerups
	Texture* pTexture5 = pResourceManager->Load<Texture>("Textures\\testPowerUp.png"); //Generic Powerup test object
	Texture* pTexture6 = pResourceManager->Load<Texture>("Textures\\quickShotPanel.png"); // Quick shot power up panel
	
	//Bullets
	Texture* pTexture7 = pResourceManager->Load<Texture>("Textures\\Bullet.png"); // Regular bullet style
	Texture* pTexture8 = pResourceManager->Load<Texture>("Textures\\QuickShotBullet.png"); // Bullet Style For The Quickshot.
	

	//Amount of ENEMIES
	const int ROCK = 30;
	const int ESHIP = 21;
	const int MOON = 200;


	double xPositions[ROCK];

	//double xPositions[POWER];

	double xPositions1[ESHIP] =
	{
		0.25, 0.2, 0.3,
		0.75, 0.8, 0.7,
		0.3, 0.25, 0.35, 0.2, 0.4,
		0.7, 0.75, 0.65, 0.8, 0.6,
		0.5, 0.4, 0.6, 0.45, 0.55
	};

	//Rock random Positions on maps of 0-1
	for (int i = 0; i < ROCK; i++)
	{
		xPositions[i] = (double)rand() / RAND_MAX;
	}
	
	double xPositions2[MOON] =
	{
		0.1, 0.2, 0.3, 0.4, 0.6, 0.7, 0.8, 0.9, //wave 1
		0.9, 0.8, 0.6, 0.5, 0.4, 0.3, 0.2, 0.1, //wave 2
		0.1, 0.2, 0.3, 0.4, 0.5, 0.6, 0.7, 0.8, 0.9, //wave 3
		0.9, 0.8, 0.7, 0.6, 0.5, 0.4, 0.3, 0.2, 0.1, //wave 4
		0.1, 0.2, 0.3, 0.4, 0.5, 0.6, 0.7, 0.8, 0.9 //wave 5

	}; // Skip 5 for boss

	double delays[ROCK];

	double delays1[ESHIP] =
	{
		0.1, 0.25, 0.25,
		3.0, 0.25, 0.25,
		3.25, 0.25, 0.25, 0.25, 0.25,
		3.25, 0.25, 0.25, 0.25, 0.25,
		3.5, 0.3, 0.3, 0.3, 0.3
	};

	double delays2[MOON] =
	{
		0.1, 0.2, 0.3, 0.4, 0.5, 0.6, 0.7, 0.8, //wave 1
		3.0, 3.2, 3.3, 3.4, 3.5, 3.6, 3.7, 3.8, //wave 2
		5.0, 5.5, 5.0, 5.5, 5.0, 5.5, 5.0, 5.5, 5.0, //wave 3
		6.0, 6.5, 6.0, 6.5, 6.0, 6.5, 6.0, 6.5, 6.0, //wave 4
		7.0, 7.5, 7.0, 7.5, 7.0, 7.5, 7.0, 7.5, 7.0 //wave 5

	};

	//Rock random Delays of 0-1
	for (int i = 0; i < ROCK; i++)
	{
		delays[i] = (double)rand() / (RAND_MAX);
	}

	//Changes the time before enemy shows up when clicking start
	float delay = 1.0; // start delay
	float delay1 = 2.0;
	float delay2 = 4.0;
	Vector2 position; // can share the Vector2

	/////////////////////////////////Creating the Enemy ships and Asteroids////////////////////////////////
	//For the asteroids
	for (int i = 0; i < ROCK; i++)
	{
		delay += delays[i];
		position.Set(xPositions[i] * Game::GetScreenWidth(), -pTexture->GetCenter().Y);
		BioEnemyShip *pEnemy = new BioEnemyShip();
		pEnemy->SetTexture(pTexture);
		pEnemy->SetCurrentLevel(this);
		pEnemy->Initialize(position, (float)delay);
		AddGameObject(pEnemy);
	}

	//For the bioenemy ships
	for (int i = 0; i < ESHIP; i++)
	{
		delay1 += delays1[i];
		position.Set(xPositions1[i] * Game::GetScreenWidth(), -pTexture1->GetCenter().Y);
		BioEnemyShip1 *pEnemy1 = new BioEnemyShip1();
		pEnemy1->SetTexture(pTexture1);
		pEnemy1->SetCurrentLevel(this);
		pEnemy1->Initialize(position, (float)delay1);
		AddGameObject(pEnemy1);
	}


	//Creating boss ship
	delay1 += 2; //Show up 0 seconds after last BioEnemyShip
	position.Set(0.5 * Game::GetScreenWidth(), pTexture2->GetCenter().Y);
	BioEnemyBoss* pEnemy2 = new BioEnemyBoss();
	pEnemy2->SetTexture(pTexture2);
	pEnemy2->SetCurrentLevel(this);
	pEnemy2->Initialize(position, delay1);
	AddGameObject(pEnemy2);

	//Creating Boss Energy Shield
	delay1 += 1;
	position.Set(0.5 * Game::GetScreenWidth(), pTexture3->GetCenter().Y + 135);
	EnemyBossWeapon1* pEnemy3 = new EnemyBossWeapon1();
	pEnemy3->SetTexture(pTexture3);
	pEnemy3->SetCurrentLevel(this);
	pEnemy3->Initialize(position, delay1);
	AddGameObject(pEnemy3);

	//For the bioenemymoon

	for (int i = 0; i < ROCK; i++)
	{
		delay2 = delays2[i] + delay1;
		position.Set(xPositions2[i] * Game::GetScreenWidth(), -pTexture4->GetCenter().Y);
		BioEnemyMoon* pEnemy4 = new BioEnemyMoon();
		pEnemy4->SetTexture(pTexture4);
		pEnemy4->SetCurrentLevel(this);
		pEnemy4->Initialize(position, (float)delay2);
		AddGameObject(pEnemy4);
	}


	Level::LoadContent(pResourceManager);
}

