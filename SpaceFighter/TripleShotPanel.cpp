#include "TripleShotPanel.h"

TripleShotPanel::TripleShotPanel()
{
	SetSpeed(300);
	SetCollisionRadius(30);

	m_hitframes = 0;

}

void TripleShotPanel::Update(const GameTime* pGameTime)
{
	// If alive
	if (IsActive())
	{
		float x = sin(pGameTime->GetTotalTime() * Math::PI + GetIndex()); //Check time since game starts
		x *= GetSpeed() * pGameTime->GetTimeElapsed() * .1f; //Set enemy ship wiggle distance and speed at which they wiggle //Normal is 1.4
		TranslatePosition(x, GetSpeed() * pGameTime->GetTimeElapsed());

		if (!IsOnScreen()) Deactivate(); // if not on screen deactivate
	}

	TripleShotPanel::Update(pGameTime);
}


void TripleShotPanel::Draw(SpriteBatch* pSpriteBatch)
{
	if (IsActive())
	{
		pSpriteBatch->Draw(m_pTexture, GetPosition(), Color::White, m_pTexture->GetCenter(), Vector2::ONE, Math::PI, 1);
	}
}
