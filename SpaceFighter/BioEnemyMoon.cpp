#include "BioEnemyMoon.h"


BioEnemyMoon::BioEnemyMoon()
{
	SetSpeed(650);
	SetMaxHitPoints(9);
	SetCollisionRadius(50);

	m_hitFrames = 0;
}

void BioEnemyMoon::Hit(const float damage)
{
	m_hitFrames += 5;
	EnemyShip::Hit(damage);
}

void BioEnemyMoon::Update(const GameTime* pGameTime)
{
	if (IsActive())
	{
		if (m_hitFrames) m_hitFrames--;
		float x = sin(pGameTime->GetTotalTime() * Math::PI + GetIndex());
		x *= 0 * pGameTime->GetTimeElapsed() * 1.4f;
		TranslatePosition(x, GetSpeed() * pGameTime->GetTimeElapsed());

		if (!IsOnScreen()) Deactivate();
	}

	EnemyShip::Update(pGameTime);
}


void BioEnemyMoon::Draw(SpriteBatch* pSpriteBatch)
{
	if (IsActive())
	{
		Color color = (m_hitFrames) ? Color::Red : Color::White;
		pSpriteBatch->Draw(m_pTexture, GetPosition(), color, m_pTexture->GetCenter(), Vector2::ONE, 0, 1);
	}
}