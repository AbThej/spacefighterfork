#pragma once

#include "PowerUp.h"

class QuickShotPanel: public PowerUp
{

public:

	QuickShotPanel();

	virtual ~QuickShotPanel() { }

	void SetTexture(Texture* pTexture) { m_pTexture = pTexture; }

	virtual void Update(const GameTime* pGameTime);

	virtual void Draw(SpriteBatch* pSpriteBatch);


private:

	Texture* m_pTexture;

	int m_hitframes;

};

