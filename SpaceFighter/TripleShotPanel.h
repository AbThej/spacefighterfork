#pragma once

#include "PowerUp.h"

class TripleShotPanel : public PowerUp
{

public:

	TripleShotPanel();

	virtual ~TripleShotPanel() { }

	void SetTexture(Texture* pTexture) { m_pTexture = pTexture; }

	virtual void Update(const GameTime* pGameTime);

	virtual void Draw(SpriteBatch* pSpriteBatch);


private:

	Texture* m_pTexture;

	int m_hitframes;


};

